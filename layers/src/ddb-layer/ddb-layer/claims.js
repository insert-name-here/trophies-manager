const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const uuid = require('uuid').v4;

const CLAIMS_TABLE = process.env.CLAIMS_TABLE_NAME;

let self = {
    createEntry: ({ projectId, email, trophies, depositAccount }) => {
        return new Promise((resolve, reject) => {
            let claim = {
                claimId: uuid(),
                projectId,
                email,
                trophies,
                depositAccount,
                isAuthorized: false,
                isComplete: false,
                timestamp: Date.now(),
            }
            console.log(`generating claim`, claim);
            dynamodb.put({
                TableName: CLAIMS_TABLE,
                Item: claim,
            }).promise()
            .then(() => {
                resolve(claim);
            })
            .catch(reject);
        });
    },
    getEntry: ({ projectId, claimId }) => {
        return new Promise((resolve, reject) => {
            dynamodb.get({
                TableName: CLAIMS_TABLE,
                Key: {
                    projectId,
                    claimId,
                }
            }).promise()
            .then(result => {
                if (result.Item) {
                    resolve(result.Item );
                } else {
                    reject(new Error(`Claim not found.`));
                }
            })
            .catch(reject);
        });
    },
};

module.exports = self;