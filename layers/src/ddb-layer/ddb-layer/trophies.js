const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const uuid = require('uuid').v4;

const TROPHIES_TABLE = process.env.TROPHIES_TABLE_NAME;
const TROPHY_TABLE_GSI = process.env.TROPHY_TABLE_GSI;

let self = {
    createEntry: ({ projectId, author, sequenceNumber, justification }) => {
        return new Promise((resolve, reject) => {
            let trophy = {
                trophyId: uuid(),
                projectId,
                author,
                sequenceNumber,
                owner: author,
                justification,
                redeemed: 0,
                timestamp: Date.now(),
            }
            console.log(`issuing trophy`, trophy);
            dynamodb.put({
                TableName: TROPHIES_TABLE,
                Item: trophy,
            }).promise()
            .then(() => {
                resolve(trophy);
            })
            .catch(reject);
        });
    },
    getEntry: ({ trophyId }) => {
        return new Promise((resolve, reject) => {
            dynamodb.query({
                TableName: TROPHIES_TABLE,
                IndexName: TROPHY_TABLE_GSI,
                KeyConditionExpression: 'trophyId = :trophy_id',
                ExpressionAttributeValues: { ':trophy_id': trophyId }
            }).promise()
            .then(result => {
                if (result.Items.length == 1) {
                    // resolve with the first item
                    resolve(result.Items[0]);
                } else {
                    if (result.Items.length > 1) {
                        // the unimaginable
                        console.error(`multiple results returned for trophy ${trophyId}`);
                    }
                    reject(new Error(`Trophy not found.`));
                }
            })
            .catch(reject);
        });
    },
    listByProjectId: ({projectId}) => {
        return new Promise(async (resolve, reject) => {
            let queryFunction = async (queryParams) => {
                return await dynamodb.query(queryParams).promise();
            };

            // initial params do not include pagination key
            let queryParams = {
                TableName: TROPHIES_TABLE,
                KeyConditionExpression: 'projectId = :project_id',
                ExpressionAttributeValues: { ':project_id': projectId }
            };

            try {
                let done = false;
                let result = [];
                let pageResult;
                while (!done) {
                    pageResult = await queryFunction(queryParams);
                    // add items to result (in place, thank you spread operator!)
                    result.push(...pageResult.Items);
                    if (pageResult.LastEvaluatedKey && pageResult.LastEvaluatedKey != "undefined") {
                        queryParams.ExclusiveStartKey = pageResult.LastEvaluatedKey;
                    } else {
                        done = true;
                    }
                }
                resolve(result);
            } catch (err) {
                reject(err);
            }
        });
    },
    updateEntry: ({ trophyId, ownerEmail, newOwnerEmail, redeemed }) => {
        return new Promise((resolve, reject) => {
            self.getEntry({ trophyId })
            .then(trophy => {
                if (trophy.owner != ownerEmail) {
                    reject(new Error(`Trophy update denied, ${ownerEmail} is not the current owner.`));
                } else {
                    if (newOwnerEmail) {
                        trophy.owner = newOwnerEmail;
                    }
                    if (redeemed) {
                        trophy.redeemed = redeemed;
                    }
                    dynamodb.put({
                        TableName: TROPHIES_TABLE,
                        Item: trophy,
                    }).promise()
                    .then(resolve)
                    .catch(reject);
                }
            })
            .catch(reject);
        });
    },
};

module.exports = self;