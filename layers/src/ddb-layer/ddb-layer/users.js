const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();

const USERS_TABLE = process.env.USERS_TABLE_NAME;

let self = {
    createEntry: ({ email, password }) => {
        return new Promise((resolve, reject) => {
            email = email.trim().toLowerCase();

            // verify that project is not already registered
            self.getEntry({ email })
            .catch(err=>{
                if (err.message != "User not found.") {
                    return reject(err);
                }
                return null;
            })
            .then(user => {
                if (user && user.confirmed) {
                    return reject(new Error(`Email has already been registered.`));
                }

                // create the user entry
                dynamodb.put({
                    TableName: USERS_TABLE,
                    Item: {
                        email,
                        password,
                        projects: [],
                        confirmed: false,
                    }
                }).promise()
                .then(() => {
                    resolve();
                })
                .catch(reject);
            });
        });
    },
    getEntry: ({ email }) => {
        return new Promise((resolve, reject) => {
            email = email.trim().toLowerCase();
            dynamodb.get({
                TableName: USERS_TABLE,
                Key: {
                    email
                }
            }).promise()
            .then(result => {
                if (result.Item) {
                    resolve(result.Item );
                } else {
                    reject(new Error(`User not found.`));
                }
            })
            .catch(reject);
        });
    },
    updateEntry: ({ email, password, projects, confirmed }) => {
        return new Promise((resolve, reject) => {
            let user = {
                email,
                password,
                projects,
                confirmed,
            };
            dynamodb.put({
                TableName: USERS_TABLE,
                Item: user
            }).promise()
            .then(() => {
                resolve(user);
            })
            .catch(reject);
        });
    },
};

module.exports = self;