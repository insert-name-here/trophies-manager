const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const sfet = require('simple-free-encryption-tool');
const uuid = require('uuid').v4;

const PROJECTS_TABLE = process.env.PROJECTS_TABLE_NAME;
const PROJECT_FUNDING_TABLE = process.env.PROJECT_FUNDING_TABLE_NAME;
const USERS_TABLE = process.env.USERS_TABLE_NAME;

const ERROR_USER_NOT_OWNER = "User not in project owners list.";

let self = {
    createEntry: ({ projectId, owners, name, currency }) => {
        return new Promise((resolve, reject) => {
            projectId = projectId || uuid();
            self.getEntry({ projectId })
            .catch(err => {
                if (err.message != "Project not found.") {
                    return reject(err);
                }
                return null;
            })
            .then(project => {
                if (project) {
                    return reject(new Error(`Project ID has already been registered.`));
                }
                let promises = [];

                // ensure owner emails are all trimmed and lowecased
                owners = owners.map(email=>email.trim().toLowerCase());

                // create project
                project = {
                    projectId,
                    owners,
                    name,
                    repositories: {},
                    tokensIssued: 0,
                    currency,
                    fundApiKey: sfet.utils.randomstring.generate(64),
                    totalFundsReceived: 0,
                };
                promises.push(dynamodb.put({
                    TableName: PROJECTS_TABLE,
                    Item: project
                }).promise());

                // update any existing owners
                for (let i in owners) {
                    promises.push(self.updateOwner({ projectId, email: owners[i], isOwner: true }));
                }

                Promise.all(promises)
                .then(()=>{
                    resolve(project);
                })
                .catch(reject);
            })
            .catch(reject);
        });
    },
    claimEntry: ({ user, projectId }) => {
        return new Promise((resolve, reject) => {
            self.getEntry({ projectId })
            .then(project=>{
                if (project.owners.indexOf(user.email) < 0) {
                    return reject(new Error(ERROR_USER_NOT_OWNER));
                }
                if (user.projects.indexOf(projectId) > -1) {
                    return reject(new Error("Project already claimed by user."));
                }
                user.projects.push(projectId);
                dynamodb.put({
                    TableName: USERS_TABLE,
                    Item: user
                }).promise()
                .then(resolve)
                .catch(reject);
            })
            .catch(reject);
        });
    },
    getEntry: ({ projectId }) => {
        return new Promise((resolve, reject) => {
            dynamodb.get({
                TableName: PROJECTS_TABLE,
                Key: {
                    projectId
                }
            }).promise()
            .then(result => {
                if (result.Item) {
                    resolve(result.Item );
                } else {
                    reject(new Error(`Project not found.`));
                }
            })
            .catch(reject);
        });
    },
    list: () => {
        return new Promise(async (resolve, reject) => {
            let scanFunction = async (scanParams) => {
                return await dynamodb.scan(scanParams).promise();
            };

            // initial params do not include pagination key
            let scanParams = {
                TableName: PROJECTS_TABLE,
            };

            try {
                let done = false;
                let result = [];
                let pageResult;
                while (!done) {
                    pageResult = await scanFunction(scanParams);
                    // add items to result (in place, thank you spread operator!)
                    result.push(...pageResult.Items);
                    if (pageResult.LastEvaluatedKey && pageResult.LastEvaluatedKey != "undefined") {
                        scanParams.ExclusiveStartKey = pageResult.LastEvaluatedKey;
                    } else {
                        done = true;
                    }
                }
                resolve(result);
            } catch (err) {
                reject(err);
            }
        });
    },
    addRepository: ({ project, repositoryUrl }) => {
        return new Promise((resolve, reject) => {
            // compatibility check
            if (Array.isArray(project.repositories)) {
                project.repositories = {};
            }

            repositoryUrl = repositoryUrl.toLowerCase();

            // check for repositoryUrl, if it doesn't already exist then generate an api key for it
            for (let repositoryId in project.repositories) {
                if (project.repositories[repositoryId].url == repositoryUrl) {
                    return reject(new Error("Repository already registered."));
                }
            }

            let repositoryId = uuid();
            project.repositories[repositoryId] = {
                repositoryId,
                url: repositoryUrl,
                apiKey: sfet.utils.randomstring.generate(64),
            }

            dynamodb.put({
                TableName: PROJECTS_TABLE,
                Item: project
            }).promise()
            .then(() => {
                resolve(project.repositories[repositoryId]);
            })
            .catch(reject);
        });
    },
    renewRepositoryApiKey: ({ project, repositoryId }) => {
        return new Promise((resolve, reject) => {
            // compatibility check
            if (Array.isArray(project.repositories)) {
                project.repositories = {};
            }

            if (!project.repositories[repositoryId]) {
                return reject(new Error("Repository not found."));
            }

            project.repositories[repositoryId].apiKey = sfet.utils.randomstring.generate(64);

            dynamodb.put({
                TableName: PROJECTS_TABLE,
                Item: project
            }).promise()
            .then(() => {
                resolve(project.repositories[repositoryId]);
            })
            .catch(reject);
        });
    },
    removeRepository: ({ project, repositoryId }) => {
        return new Promise((resolve, reject) => {
            // compatibility check
            if (Array.isArray(project.repositories)) {
                project.repositories = {};
            }

            if (!project.repositories[repositoryId]) {
                return reject(new Error("Repository not found."));
            }

            delete project.repositories[repositoryId];

            dynamodb.put({
                TableName: PROJECTS_TABLE,
                Item: project
            }).promise()
            .then(resolve)
            .catch(reject);
        });
    },
    renewFundApiKey: ({ email, projectId }) => {
        return new Promise((resolve, reject) => {
            self.getEntry({ projectId })
            .then(project=>{
                if (project.owners.indexOf(email) < 0) {
                    return reject(new Error(ERROR_USER_NOT_OWNER));
                }

                project.fundApiKey = sfet.utils.randomstring.generate(64);

                dynamodb.put({
                    TableName: PROJECTS_TABLE,
                    Item: project
                }).promise()
                .then(() => {
                    resolve(project);
                })
                .catch(reject);
            })
            .catch(reject);
        });
    },
    updateEntry: ({ projectId, tokensIssued, owners, currency, repositories, fundApiKey, totalFundsReceived, name }) => {
        return new Promise((resolve, reject) => {
            // get the existing project entry to perform a diff on the owners
            self.getEntry({ projectId })
            .then(project => {
                let promises = [];
                // update the project entry
                promises.push(dynamodb.put({
                    TableName: PROJECTS_TABLE,
                    Item: {
                        projectId,
                        tokensIssued,
                        owners,
                        currency,
                        repositories,
                        fundApiKey,
                        totalFundsReceived,
                        name,
                    }
                }).promise());

                // get owners diff, add or remove as necessary
                for (let i in project.owners) {
                    // if existing owner is not in the updated owners list, remove the project from the user
                    if (owners.indexOf(project.owners[i]) < 0) {
                        promises.push(self.updateOwner({ projectId, email: project.owners[i], isOwner: false }));
                    }
                }
                for (let i in owners) {
                    // if new owner is not in the existing owners list, add the project to the user
                    if (project.owners.indexOf(owners[i]) < 0) {
                        promises.push(self.updateOwner({ projectId, email: owners[i], isOwner: true }));
                    }
                }

                Promise.all(promises)
                .then(resolve)
                .catch(reject);
            })
            .catch(reject);
        });
    },
    updateFunding: ({ projectId, funds, source }) => {
        return new Promise((resolve, reject) => {
            dynamodb.put({
                TableName: PROJECT_FUNDING_TABLE,
                Item: {
                    projectId,
                    funds,
                    source,
                    timestamp: Date.now(),
                }
            }).promise()
            .then(resolve)
            .catch(reject);
        });
    },
    updateOwner: ({ projectId, email, isOwner }) => {
        return new Promise((resolve, reject) => {
            dynamodb.get({
                TableName: USERS_TABLE,
                Key: {
                    email
                }
            }).promise()
            .then(result => {
                if (result.Item) {
                    let user = result.Item;
                    let isUpdateRequired = false;
                    projectIndex = user.projects.indexOf(projectId);
                    if (isOwner && projectIndex < 0) {
                        isUpdateRequired = true;
                        user.projects.push(projectId);
                    }
                    if (!isOwner && projectIndex > -1) {
                        isUpdateRequired = true;
                        user.projects.splice(projectIndex, 1);
                    }
                    if (isUpdateRequired) {
                        dynamodb.put({
                            TableName: USERS_TABLE,
                            Item: user
                        }).promise()
                        .then(() => {
                            resolve(user);
                        })
                        .catch(reject);
                    } else {
                        // no ownership change, no update required
                        resolve();
                    }
                } else {
                    // no user found, no update required
                    resolve();
                }
            })
            .catch(reject);
        });
    },
};

module.exports = self;