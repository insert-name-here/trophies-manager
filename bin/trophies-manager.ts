#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { TrophiesManagerStack } from '../lib/trophies-manager-stack';
import stacksJson from '../lib/stacks.json';

const app = new cdk.App();
cdk.Tags.of(app).add("app", "trophies");

type stackType = {
  "corsOrigin": string,
  "region": {
      "region": string,
      "account": string,
  },
  "jwt": {
      "secret": string,
      "authTokenExpiration": string,
      "refreshTokenExpiration": string
  },
  "mailgun": {
    "domain": string,
    "from": string,
    "api_key": string
  }
}
type stacksType = {
  [key: string]: stackType
}

let stacks:stacksType = stacksJson;

for (let name in stacks) {
  let stack:stackType = stacks[name];
  let regionOptions;
  let stackName = `TrophiesStack-${name}`;
  if (!stack.region) {
      // deploy region-agnostic when no region is specified
      regionOptions = undefined;
  } else {
      regionOptions = { env: stack.region };
  }
  let stackInstance = new TrophiesManagerStack(
      app, stackName,
      regionOptions,
      stack
  );
  cdk.Tags.of(stackInstance).add('stack-name', stackName);
}
