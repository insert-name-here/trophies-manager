# Trophies manager

A serverless token issuing and tracking solution for [Trophies](https://gitlab.com/insert-name-here/trophies).

## Solution overview

1. When a PR is merged, a merge hook makes a request to the trophies manager with the authorized repository's URL and the commit hash.
2. The trophies manager looks for the `trophies.tracking.json` file for the specified commit and issues trophies accordingly.
3. Project funds are added to the project by authorized clients.
4. When a trophy owner would like to redeem funds, they submit a claim request listing the trophies against which they wish to redeem funds and a wallet address or account details where they would like the funds to be deposited.
    a. Trophies in the claim request are grouped into separate claims by project.
    b. A claim that includes trophies that are eligible for redemption (trophies that have not already redeemed more than their share of the project's total funds over time) is submitted to the relevant project owners by email with a confirmation code.
    c. If the project's funds are available, the confirmation code must be submitted to authorize the claim.
    d. Once funds have been transferred, claims must be marked a completed and the claim's trophies updated accordingly.

## Useful commands

- `npm run build`   build layers and compile typescript to js
- `npm run synth`   perform build steps then synthesize the CloudFormation template(s)
- `cdk deploy`      deploy this stack to your default AWS account/region
- `cdk diff`        compare deployed stack with current state

## JWT secret generation

Run `node generateJwtSecret.js` in the root folder to generate a JWT secret for the `/lib/stacks.json` file.

## API documentation

TBD: autogenerate openapi docs

### Registration

#### POST {{host}}/register

Request format:

```json
{
    "email": "abcd@example.com",
    "password": "my super-secret password"
}
```

Response format:

```json
{
    "success": true,
    "message": "Please confirm your registration using the OTP sent to your email address."
}
```

#### POST {{host}}/register/confirm

Request format:

```json
{
    "email": "abcd@example.com",
    "otp": "8S54Fc"
}
```

Response format:

```json
{
    "success": true,
    "message": "Account confirmation succeeded."
}
```

### Password Reset

#### POST {{host}}/reset-password/request

Request format:

```json
{
    "email": "abcd@example.com"
}
```

Response format:

```json
{
    "success": true,
    "message": "Please check your email inbox for the OTP required to reset your password."
}
```

#### POST {{host}}/reset-password

Request format:

```json
{
    "email": "abcd@example.com",
    "otp": "sILGGs",
    "password": "a different super-secret password"
}
```

Response format:

```json
{
    "success": true,
    "message": "Password has been updated successfully."
}
```

### Authentication

#### POST {{host}}/login

Request format:

```json
{
    "email": "abcd@example.com",
    "password": "my super-secret password",
    "deviceId": "a string identifier for your device"
}
```

Response format:

```json
{
    "email": "abcd@example.com",
    "authToken": "XXXXXXXXXXXX",
    "authTokenExpiration": "30m",
    "refreshToken": "63ea0860-de96-4373-b8e2-663d0bb8cc46",
    "refreshTokenExpiration": "14d",
    "deviceId": "a string identifier for your device"
}
```

#### POST {{host}}/login/refresh

Request format:

```json
{
    "email": "abcd@example.com",
    "refreshToken": "63ea0860-de96-4373-b8e2-663d0bb8cc46",
    "deviceId": "a string identifier for your device"
}
```

Response format:

```json
{
    "email": "abcd@example.com",
    "authToken": "XXXXXXXXXXXX",
    "authTokenExpiration": "30m",
    "refreshToken": "63ea0860-de96-4373-b8e2-663d0bb8cc46",
    "refreshTokenExpiration": "14d",
    "deviceId": "a string identifier for your device"
}
```

### Project Management

#### POST {{host}}/projects

BEARER TOKEN REQUIRED.

Request format:

```json
{
    "owners": [ "abcd@example.com", "efgh@example.com" ],
    "name": "Demo project",
    "currency": "ZAR"
}
```

Response format:

```json
{
    "projectId": "ec412f19-a197-4ba8-8e62-b92e53e38262",
    "tokensIssued": 0,
    "owners": [
        "abcd@example.com",
        "efgh@example.com"
    ],
    "currency": "ZAR",
    "repositories": {},
    "totalFundsReceived": 0,
    "name": "Demo project",
    "fundApiKey": "OXN6lFdfrHUMMUSraodjBuhqLv5Z8jsFg3EFuQZZcNU8v2fftbrfpBBUz1PLEAZE"
}
```

#### GET {{host}}/projects

BEARER TOKEN OPTIONAL.

Response format:

```json
[
    {
        "projectId": "ec412f19-a197-4ba8-8e62-b92e53e38262",
        "tokensIssued": 0,
        "owners": [
            "abcd@example.com",
            "efgh@example.com"
        ],
        "currency": "ZAR",
        "repositories": [],
        "totalFundsReceived": 0,
        "name": "Demo project"
    },
    ...
]
```

A project owner will see the following `repositories` structure in addition to a `fundApiKey`:

```json
    ...
        "repositories": {
            "4e2f3828-9e2d-43fd-a333-d5f4d507f0fa": {
                "repositoryId": "4e2f3828-9e2d-43fd-a333-d5f4d507f0fa",
                "apiKey": "ZkqwTnVMgXfMcso7o4laZ7NzSsxERmVaW1JcOfDasAatVj9BAV9PZQGByHCUmvNQ",
                "url": "https://gitlab.com/gitlab-user/demo-project"
            }
        },
    ...
```

Whereas a non-owner will see the following:

```json
    ...
        "repositories": [
            "https://gitlab.com/gitlab-user/demo-project"
        ],
    ...
```

#### GET {{host}}/projects/{{projectId}}

BEARER TOKEN OPTIONAL.

Response format:

```json
{
    "projectId": "ec412f19-a197-4ba8-8e62-b92e53e38262",
    "tokensIssued": 0,
    "owners": [
        "abcd@example.com",
        "efgh@example.com"
    ],
    "currency": "ZAR",
    "repositories": [],
    "totalFundsReceived": 0,
    "name": "Demo project"
}
```

A project owner will see the following `repositories` structure in addition to a `fundApiKey`:

```json
    ...
        "repositories": {
            "4e2f3828-9e2d-43fd-a333-d5f4d507f0fa": {
                "repositoryId": "4e2f3828-9e2d-43fd-a333-d5f4d507f0fa",
                "apiKey": "ZkqwTnVMgXfMcso7o4laZ7NzSsxERmVaW1JcOfDasAatVj9BAV9PZQGByHCUmvNQ",
                "url": "https://gitlab.com/gitlab-user/demo-project"
            }
        },
    ...
```

Whereas a non-owner will see the following:

```json
    ...
        "repositories": [
            "https://gitlab.com/gitlab-user/demo-project"
        ],
    ...
```

#### POST {{host}}/projects/{{projectId}}/claim

BEARER TOKEN REQUIRED.

Authenticated user claims project ownership for a project in which they are already a registered owner.

Response format:

```json
{
    "success": true
}
```

#### POST {{host}}/projects/{{projectId}}/funds/renewToken

BEARER TOKEN REQUIRED.

Authenticated user renews the project's funding API key.

Response format:

```json
{
    "projectId": "ec412f19-a197-4ba8-8e62-b92e53e38262",
    "tokensIssued": 0,
    "owners": [
        "abcd@example.com",
        "efgh@example.com"
    ],
    "currency": "ZAR",
    "repositories": {},
    "totalFundsReceived": 0,
    "name": "Demo project",
    "fundApiKey": "OXN6lFdfrHUMMUSraodjBuhqLv5Z8jsFg3EFuQZZcNU8v2fftbrfpBBUz1PLEAZE"
}
```

#### POST {{host}}/projects/{{projectId}}/funds

Authorized caller adds funds to project.

PROJECT'S FUNDING API KEY (`fundApiKey`) REQUIRED (Header: `apiKey`)

Request format:

```json
{
    "funds": 1000, // funds in smallest denomination of project currency
    "source": "A string identifying the source of the funds"
}
```

Response format:

```json
{
    "success": true,
    "totalFundsReceived": 2000
}
```

#### POST {{host}}/projects/{{projectId}}/repositories

BEARER TOKEN REQUIRED.

Request format:

```json
{
    "url": "https://gitlab.com/gitlab-user/demo-project"
}
```

Response format:

```json
{
    "success": true,
    "repository": {
        "repositoryId": "e0606dd8-3e97-4fb3-886e-d7b557e159de",
        "url": "https://gitlab.com/gitlab-user/demo-project",
        "apiKey": "OBvlgNLVXMtxGsFgMNqETsWWBW3RE8p1psWAUMNg9troQelnfKaGBHTbKppsF0iU"
    }
}
```

### PUT {{host}}/projects/{{projectId}}/repositories/{{repositoryId}}

BEARER TOKEN REQUIRED.

Response format:

```json
{
    "success": true,
    "repository": {
        "repositoryId": "e0606dd8-3e97-4fb3-886e-d7b557e159de",
        "apiKey": "3jdXpuDt5B3xuly30ofHAtEhfzU95gvAZpGoOStU5h3ubUAwaiJsjtvV26QWN4im",
        "url": "https://gitlab.com/gitlab-user/demo-project"
    }
}
```

### DELETE {{host}}/projects/{{projectId}}/repositories/{{repositoryId}}

BEARER TOKEN REQUIRED.

Response format:

```json
{
    "success": true
}
```

### Trophies

#### POST {{host}}/projects/{{projectId}}/trophies

HTTP HEADER "apiKey" REQUIRED.

Trophies can be issued manually by a logged in project owner, or automatically by a repository on PR merge.

Request format:

```json
{
  "author": "abcd@example.com",
  "trophies": 1,
  "justification": "https://gitlab.com/gitlab-user/demo-project/-/tree/feature/branch-full-of-magic"
}
```

Response format:

```json
{
    "success": true,
    "message": "Trophies issued successfully."
}
```

#### GET {{host}}/trophies/{{trophyId}}

Response format:

```json
{
    "trophyId": "12e9984f-796e-4cae-915d-55f923533232",
    "projectId": "ec412f19-a197-4ba8-8e62-b92e53e38262",
    "author": "abcd@example.com",
    "sequenceNumber": "1/1",
    "owner": "abcd@example.com",
    "justification": "https://gitlab.com/gitlab-user/demo-project/-/tree/feature/branch-full-of-magic",
    "timestamp": 1623787213974,
}
```

#### GET {{host}}/projects/{{projectId}}/trophies

Response format:

```json
[
    {
        "trophyId": "12e9984f-796e-4cae-915d-55f923533232",
        "projectId": "ec412f19-a197-4ba8-8e62-b92e53e38262",
        "author": "abcd@example.com",
        "sequenceNumber": "1/1",
        "owner": "abcd@example.com",
        "justification": "https://gitlab.com/gitlab-user/demo-project/-/tree/feature/branch-full-of-magic",
        "timestamp": 1623787213974,
    }
    ...
]
```

#### POST {{host}}/trophies/redeem

BEARER TOKEN REQUIRED.

Request format:

```json
{
    "trophies": [ "12e9984f-796e-4cae-915d-55f923533232" ],
    "depositAccount": "a wallet address or account details"
}
```
