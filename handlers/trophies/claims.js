
const jwt = require('/opt/nodejs/utility-layer/jwt');
const otp = require('/opt/nodejs/otp-layer/otp');
const projects = require('/opt/nodejs/ddb-layer/projects');
const trophies = require('/opt/nodejs/ddb-layer/trophies');
const claims = require('/opt/nodejs/ddb-layer/claims');
const utils = require('/opt/nodejs/utility-layer/utils');

const PURPOSE_CLAIM_AUTHORIZATION = "claim authorization";

function sendClaimAuthorizationOTP({ claim, email, owners }) {
    return new Promise((resolve, reject) => {
        otp.createEntry({ email, purpose: PURPOSE_CLAIM_AUTHORIZATION })
        .then((value) => {
            let notificationEmails = [];
            notificationEmails.push(
                otp.send({
                    email: owners.join(),
                    subject: "A Trophies payout requires your authorization!",
                    body: `Please submit the following One-Time Password along with the claimId to authorize the following claim:<br />
<br />
Claim: <pre>${JSON.stringify(claim, null, 4)}</pre>
<br />
Your authorization OTP: ${value}
<br />
`
                })
            );

            notificationEmails.push(
                otp.send({
                    email,
                    subject: "Your Trophies payout claim has been registered!",
                    body: `The following claim has been submitted:<br />
<br />
Claim: <pre>${JSON.stringify(claim, null, 4)}</pre>
<br />
`
                })
            );

            Promise.all(notificationEmails)
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
}

exports.requestTrophyRedemption = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(({ email }) => {
            try {
                let payload = utils.processEventBody({
                    body: event.body,
                    required: [ 'trophies', 'depositAccount' ]
                });

                // validate payload
                if (!Array.isArray(payload.trophies)) {
                    throw new Error(`trophies field must be an array`);
                }

                // get all the trophies being claimed
                let trophyPromises = [];
                for (let i in payload.trophies) {
                    trophyPromises.push(
                        trophies.getEntry({ trophyId: payload.trophies[i] })
                    );
                }

                Promise.all(trophyPromises)
                .then(trophyResults => {
                    let requestedClaims = {};
                    let projectPromises = [];
                    // initialize claims and prepare to get relevant project details
                    for (let i in trophyResults) {
                        let trophy = trophyResults[i];
                        if (trophy.owner != email) {
                            return resolve(utils.formatErrorResponse(
                                403,
                                new Error(`trophy ${trophy.trophyId} not owned by user`)
                            ));
                        }
                        if (!requestedClaims[trophy.projectId]) {
                            projectPromises.push(projects.getEntry({ projectId: trophy.projectId}));
                            requestedClaims[trophy.projectId] = {
                                projectId: trophy.projectId,
                                email,
                                trophies: [],
                                depositAccount: payload.depositAccount,
                            };
                        }
                    }

                    // retrieve all relevant projects
                    Promise.all(projectPromises)
                    .then(projectResults=>{
                        let trophyValues = {};
                        let projectOwners = {};
                        // determine the values of each project's trophies
                        for (let i in projectResults) {
                            let project = projectResults[i];
                            projectOwners[project.projectId] = project.owners;
                            // get value of project trophies rounded down (funds loaded in smallest denomination)
                            trophyValues[project.projectId] = Number(project.totalFundsReceived) === 0 ? 0 :
                                Math.floor(
                                    Number(project.totalFundsReceived) / Number(project.tokensIssued)
                                );
                        }

                        // if trophy value has not yet been redeemed, add its outstanding value to claim
                        for (let i in trophyResults) {
                            let trophy = trophyResults[i];
                            trophy.redeemed = trophy.redeemed || 0;
                            let unredeemed = Number(trophyValues[trophy.projectId]) - Number(trophy.redeemed);
                            if (unredeemed > 0) {
                                requestedClaims[trophy.projectId].trophies.push({
                                    trophyId: trophy.trophyId,
                                    amount: unredeemed
                                });
                            }
                        }

                        // store valid claims and submit emails to project owners
                        let claimPromises = [];
                        for (let projectId in requestedClaims) {
                            if (requestedClaims[projectId].trophies.length > 0) {
                                claimPromises.push(claims.createEntry(requestedClaims[projectId]));
                            }
                        }

                        if (claimPromises.length === 0) {
                            console.log(`no valid claims produced`);
                            return resolve(utils.formatErrorResponse(
                                500,
                                new Error(`submitted tokens have no outstanding value`)
                            ));
                        }

                        Promise.all(claimPromises)
                        .then(claimResults=>{
                            let claimAuthorizationEmailPromises = [];
                            for (let i in claimResults) {
                                let claim = claimResults[i];
                                console.log(claim);

                                // create and send the authorization emails
                                claimAuthorizationEmailPromises.push(
                                    sendClaimAuthorizationOTP({
                                        claim,
                                        email,
                                        owners: projectOwners[claim.projectId],
                                    })
                                );
                            }

                            Promise.all(claimAuthorizationEmailPromises)
                            .then(() => {
                                resolve(utils.createResponse({
                                    "statusCode": 200,
                                    "body": {
                                        "success": true,
                                        "message": "Claims generated successfully."
                                    }
                                }));
                            })
                            .catch((err) => {
                                console.error(err);
                                resolve(utils.formatErrorResponse(
                                    500,
                                    err,
                                    `Trophy claims generated successfully but email notifications not sent.`
                                ));
                            });
                        })
                        .catch(err=>{
                            console.log(`error generating claims`, err);
                            resolve(utils.formatErrorResponse(
                                500,
                                new Error(`an unexpected error has occurred`)
                            ));
                        });
                    })
                    .catch(err=>{
                        console.log(`error retrieving claim projects`, err)
                        resolve(utils.formatErrorResponse(
                            500,
                            new Error(`an unexpected error has occurred`)
                        ));
                    });
                })
                .catch(err=>{
                    console.log(`error retrieving claim trophies`, err)
                    resolve(utils.formatErrorResponse(
                        500,
                        new Error(`one or more trophies in claim invalid`)
                    ));
                })
            } catch (err) {
                console.error(err);
                resolve(utils.formatErrorResponse(
                    400,
                    err
                ));
            }
        })
        .catch(err => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err
            ));
        });
    });
};
