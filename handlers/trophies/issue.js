
const jwt = require('/opt/nodejs/utility-layer/jwt');
const otp = require('/opt/nodejs/otp-layer/otp');
const projects = require('/opt/nodejs/ddb-layer/projects');
const trophies = require('/opt/nodejs/ddb-layer/trophies');
const utils = require('/opt/nodejs/utility-layer/utils');

exports.issueTrophies = async (event) => {
    return new Promise(async (resolve, reject) => {
        try {
            let payload = utils.processEventBody({
                body: event.body,
                required: [ 'author', 'trophies', 'justification' ]
            });

            let projectId = event.pathParameters.projectId;
            let project;
            try {
                project = await projects.getEntry({ projectId });
            } catch (err) {
                console.error(`error retrieving projectId ${projectId}:`, err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err
                ));
            }

            // TODO determine repositoryUrl (from header or post body?) and authenticate with apiKey
            let apiKey = payload.apiKey;
            if (event.headers['apiKey']) {
                apiKey = event.headers['apiKey'];
            }

            // TODO or ensure that this request is by an authenticated project owner
            let session;
            if (event.headers['Authorization']) {
                try {
                    // session.email
                    session = await jwt.authenticateToken({requestHeaders: event.headers });
                } catch (err) {
                    console.error(err);
                    return resolve(utils.formatErrorResponse(
                        401,
                        err
                    ));
                }
            }

            // TODO validate payload

            // validate trophies
            if (!Number.isInteger(payload.trophies) || payload.trophies < 1) {
                throw new Error(`Trophies must be a positive integer.`);
            }

            let creationPromises = [];
            // for each trophy, create sequenceNumber and issue it
            for (let i = 0; i < payload.trophies; i++) {
                creationPromises.push(trophies.createEntry({
                    projectId,
                    author: payload.author,
                    sequenceNumber: `${i+1}/${payload.trophies}`,
                    justification: payload.justification,
                }))
            }
            project.tokensIssued = Number(project.tokensIssued) + Number(payload.trophies);
            creationPromises.push(projects.updateEntry(project));

            Promise.all(creationPromises)
            .then(results => {
                let emailPromises = [];
                for (let i in results) {
                    let trophy = results[i];
                    // we only send emails for trophy promises
                    if (trophy.author) {
                        emailPromises.push(otp.send({
                            email: trophy.author,
                            subject: `Project ${project.name} has issued you a Trophy!`,
                            body: `Congratulations! You have been issued the following trophy:
                                <br />
                                Trophy ID: ${trophy.trophyId}
                                Sequence Number: ${trophy.sequenceNumber}
                                Issued for: ${trophy.justification}
                                Date Issued: ${new Date(trophy.timestamp).toUTCString()}
                                <br />`
                        }));
                    }
                }

                Promise.all(emailPromises)
                .then(() => {
                    resolve(utils.createResponse({
                        "statusCode": 200,
                        "body": {
                            "success": true,
                            "message": `Trophies issued successfully.`
                        }
                    }));
                })
                .catch(err=>{
                    resolve(utils.formatErrorResponse(
                        500,
                        err,
                        `Trophies issued successfully but email notifications not sent.`
                    ));
                });
            })
            .catch(err=>{
                console.error(`error issuing trophies for ${payload.projectId}:`, err);
                resolve(utils.formatErrorResponse(
                    500,
                    err
                ));
            });
        } catch (err) {
            console.error(err);
            resolve(utils.formatErrorResponse(
                400,
                err
            ));
        }
    });
};
