
const trophies = require('/opt/nodejs/ddb-layer/trophies');
const utils = require('/opt/nodejs/utility-layer/utils');

exports.getTrophy = async (event) => {
    return new Promise((resolve, reject) => {
        trophies.getEntry({ trophyId: event.pathParameters.trophyId })
        .then(trophy => {
            resolve(utils.createResponse({
                "statusCode": 200,
                "body": trophy
            }));
        })
        .catch(err => {
            resolve(utils.formatErrorResponse(
                500,
                err
            ));
        });
    });
};

exports.listProjectTrophies = async (event) => {
    return new Promise((resolve, reject) => {
        trophies.listByProjectId({ projectId: event.pathParameters.projectId })
        .then(trophies => {
            resolve(utils.createResponse({
                "statusCode": 200,
                "body": trophies
            }));
        })
        .catch(err => {
            resolve(utils.formatErrorResponse(
                500,
                err
            ));
        });
    });
};
