
const jwt = require('/opt/nodejs/utility-layer/jwt');
const projects = require('/opt/nodejs/ddb-layer/projects');
const users = require('/opt/nodejs/ddb-layer/users');
const utils = require('/opt/nodejs/utility-layer/utils');

const ERROR_USER_NOT_OWNER = "User not in project owners list.";

exports.createProject = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(({ email }) => {
            try {
                let payload = utils.processEventBody({
                    body: event.body,
                    required: [ 'owners', 'name', 'currency' ]
                });

                // TODO validate payload
                projects.createEntry(payload)
                .then(project => {
                    resolve(utils.createResponse({
                        "statusCode": 200,
                        "body": project
                    }));
                })
                .catch(err=>{
                    resolve(utils.formatErrorResponse(
                        500,
                        err
                    ));
                });
            } catch (err) {
                console.error(err);
                resolve(utils.formatErrorResponse(
                    400,
                    err
                ));
            }
        })
        .catch(err => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err
            ));
        });
    });
};

exports.getProject = async (event) => {
    return new Promise(async (resolve, reject) => {
        let session;
        if (event.headers['Authorization']) {
            try {
                // session.email
                session = await jwt.authenticateToken({requestHeaders: event.headers });
            } catch (err) {
                console.error(err);
                return resolve(utils.formatErrorResponse(
                    401,
                    err
                ));
            }
        }

        projects.getEntry({ projectId: event.pathParameters.projectId })
        .then(project => {
            if (!session || project.owners.indexOf(session.email) < 0) {
                // for public listings, only show repository urls
                let repositories = [];
                for (let repositoryId in project.repositories) {
                    repositories.push(project.repositories[repositoryId].url);
                }
                project.repositories = repositories;
                // for public listings, don't show the funding api key
                delete project.fundApiKey;
            }
            resolve(utils.createResponse({
                "statusCode": 200,
                "body": project
            }));
        })
        .catch(err => {
            resolve(utils.formatErrorResponse(
                500,
                err
            ));
        });
    });
};

exports.listProjects = async (event) => {
    return new Promise(async (resolve, reject) => {
        let session;
        if (event.headers['Authorization']) {
            try {
                // session.email
                session = await jwt.authenticateToken({requestHeaders: event.headers });
            } catch (err) {
                console.error(err);
                return resolve(utils.formatErrorResponse(
                    401,
                    err
                ));
            }
        }

        projects.list()
        .then(result => {
            resolve(utils.createResponse({
                "statusCode": 200,
                "body": result.map(project=>{
                    if (!session || project.owners.indexOf(session.email) < 0) {
                        // for public listings, only show repository urls
                        let repositories = [];
                        for (let repositoryId in project.repositories) {
                            repositories.push(project.repositories[repositoryId].url);
                        }
                        project.repositories = repositories;
                        // for public listings, don't show the funding api key
                        delete project.fundApiKey;
                    }
                    return project;
                })
            }));
        })
        .catch(err => {
            resolve(utils.formatErrorResponse(
                500,
                err
            ));
        });
    });
};

exports.claimProject = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(({ email }) => {
            let projectId = event.pathParameters.projectId;

            users.getEntry({ email })
            .then(user => {
                projects.claimEntry({ user, projectId })
                .then(()=>{
                    resolve(utils.createResponse({
                        "statusCode": 200,
                        "body": {
                            "success": true
                        }
                    }));
                })
                .catch(err=>{
                    let statusCode = (err.message === ERROR_USER_NOT_OWNER) ? 403 : 500;
                    resolve(utils.formatErrorResponse(
                        statusCode,
                        err
                    ));
                });
            })
            .catch(err=>{
                resolve(utils.formatErrorResponse(
                    500,
                    err
                ));
            });
        })
        .catch(err => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err
            ));
        });
    });
};

exports.renewFundApiKey = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(({ email }) => {
            let projectId = event.pathParameters.projectId;

            projects.renewFundApiKey({ email, projectId })
            .then(project => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": project,
                }));
            })
            .catch(err=>{
                let statusCode = (err.message === ERROR_USER_NOT_OWNER) ? 403 : 500;
                resolve(utils.formatErrorResponse(
                    statusCode,
                    err
                ));
            });
        })
        .catch(err => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err
            ));
        });
    });
};

exports.fundProject = async (event) => {
    return new Promise((resolve, reject) => {
        let projectId = event.pathParameters.projectId;
        let apiKey = event.headers.apiKey;

        projects.getEntry({ projectId })
        .then(project => {
            if (apiKey && apiKey.length > 0 && apiKey != project.fundApiKey) {
                resolve(utils.formatErrorResponse(
                    401,
                    new Error("Invalid credentials.")
                ));
            }
            // update project funds from payload body
            try {
                let payload = utils.processEventBody({
                    body: event.body,
                    required: [ 'funds', 'source' ]
                });

                // validate funds
                if (!Number.isInteger(payload.funds) || payload.funds < 1) {
                    throw new Error(`Funds must be a positive integer.`);
                }

                let promises = [];

                project.totalFundsReceived = Number(project.totalFundsReceived) + Number(payload.funds);

                promises.push(projects.updateEntry(project));
                promises.push(projects.updateFunding({
                    projectId,
                    funds: Number(payload.funds),
                    source: payload.source
                }));

                Promise.all(promises)
                .then(() => {
                    resolve(utils.createResponse({
                        "statusCode": 200,
                        "body": {
                            "success": true,
                            "totalFundsReceived": project.totalFundsReceived,
                        }
                    }));
                })
                .catch(err=>{
                    console.error(`error funding project ${projectId}:`, err);
                    resolve(utils.formatErrorResponse(
                        500,
                        err
                    ));
                });
            } catch (err) {
                console.error(err);
                resolve(utils.formatErrorResponse(
                    400,
                    err
                ));
            }
        })
        .catch(err => {
            resolve(utils.formatErrorResponse(
                500,
                err
            ));
        });
    });
};
