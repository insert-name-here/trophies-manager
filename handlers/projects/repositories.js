
const jwt = require('/opt/nodejs/utility-layer/jwt');
const projects = require('/opt/nodejs/ddb-layer/projects');
const utils = require('/opt/nodejs/utility-layer/utils');

const ERROR_USER_NOT_OWNER = "User not in project owners list.";

exports.addRepository = async (event) => {
    return new Promise(async (resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(async ({ email }) => {
            let projectId = event.pathParameters.projectId;

            let project;
            try {
                project = await projects.getEntry({ projectId });
            } catch (err) {
                console.error(`error retrieving projectId ${projectId}:`, err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err
                ));
            }

            if (project.owners.indexOf(email) < 0) {
                return resolve(utils.formatErrorResponse(
                    403,
                    new Error(ERROR_USER_NOT_OWNER)
                ));
            }

            let payload;
            try {
                payload = utils.processEventBody({
                    body: event.body,
                    required: [ 'url' ]
                });
            } catch (err) {
                return resolve(utils.formatErrorResponse(
                    400,
                    err
                ));
            }

            projects.addRepository({ project, repositoryUrl: payload.url })
            .then(repository => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        success: true,
                        repository,
                    }
                }));
            })
            .catch(err=>{
                resolve(utils.formatErrorResponse(
                    500,
                    err
                ));
            });
        })
        .catch(err => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err
            ));
        });
    });
};

exports.renewApiKey = async (event) => {
    return new Promise(async (resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(async ({ email }) => {
            let projectId = event.pathParameters.projectId;
            let repositoryId = event.pathParameters.repositoryId;

            let project;
            try {
                project = await projects.getEntry({ projectId });
            } catch (err) {
                console.error(`error retrieving projectId ${projectId}:`, err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err
                ));
            }

            if (project.owners.indexOf(email) < 0) {
                return resolve(utils.formatErrorResponse(
                    403,
                    new Error(ERROR_USER_NOT_OWNER)
                ));
            }

            projects.renewRepositoryApiKey({ project, repositoryId })
            .then(repository => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        success: true,
                        repository,
                    }
                }));
            })
            .catch(err=>{
                resolve(utils.formatErrorResponse(
                    500,
                    err
                ));
            });
        })
        .catch(err => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err
            ));
        });
    });
};

exports.removeRepository = async (event) => {
    return new Promise(async (resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(async ({ email }) => {
            let projectId = event.pathParameters.projectId;
            let repositoryId = event.pathParameters.repositoryId;

            let project;
            try {
                project = await projects.getEntry({ projectId });
            } catch (err) {
                console.error(`error retrieving projectId ${projectId}:`, err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err
                ));
            }

            if (project.owners.indexOf(email) < 0) {
                return resolve(utils.formatErrorResponse(
                    403,
                    new Error(ERROR_USER_NOT_OWNER)
                ));
            }

            projects.removeRepository({ project, repositoryId })
            .then(() => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        success: true,
                    }
                }));
            })
            .catch(err=>{
                resolve(utils.formatErrorResponse(
                    500,
                    err
                ));
            });
        })
        .catch(err => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err
            ));
        });
    });
};
