
const bcrypt = require('bcryptjs');
const jwt = require('/opt/nodejs/utility-layer/jwt');
const users = require('/opt/nodejs/ddb-layer/users');
const utils = require('/opt/nodejs/utility-layer/utils');

exports.login = async (event) => {
    return new Promise((resolve, reject) => {
        try {
            let payload = utils.processEventBody({ body: event.body, required: [ 'email', 'password' ] });
            let resolveInvalidCredentials = (msg) => {
                console.error(msg);
                return resolve(utils.formatErrorResponse(
                    401,
                    null,
                    `Invalid credentials.`
                ));
            };
            users.getEntry(payload)
            .then(user => {
                if (!user.confirmed) {
                    return resolveInvalidCredentials(
                        `unauthorized access detected, ${user.email} not confirmed`
                    );
                }
                bcrypt.compare(payload.password, user.password, function(err, result) {
                    if (err || !result) {
                        resolveInvalidCredentials(`bcrypt password match failed`, err);
                        return;
                    }
                    jwt.generateTokens({ ...user, deviceId: payload.deviceId })
                    .then(session => {
                        resolve(utils.createResponse({
                            "statusCode": 200,
                            "body": session
                        }));
                    })
                    .catch(err => {
                        console.error(`error generating jwt tokens for ${user.email}`, err);
                        return resolve(utils.formatErrorResponse(
                            500,
                            null,
                            `An unexpected error occurred.`
                        ));

                    });
                });
            })
            .catch(err => {
                resolveInvalidCredentials(
                    `an error occurred while querying ${payload.email}`, err
                );
            });
        } catch (err) {
            console.error(err);
            resolve(utils.formatErrorResponse(
                400,
                err
            ));
        }
    });
};
