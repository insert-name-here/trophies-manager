
const jwt = require('/opt/nodejs/utility-layer/jwt');
const utils = require('/opt/nodejs/utility-layer/utils');

exports.refreshToken = async (event) => {
    return new Promise((resolve, reject) => {
        let payload;
        try {
            // we receive the email and the refreshToken in the body
            payload = utils.processEventBody({ body: event.body, required: ['email', 'refreshToken'] });
        } catch (err) {
            console.error(`error parsing event body`, err, event.body);
            return resolve(utils.formatErrorResponse(
                400,
                err,
                `Invalid request format.`
            ));
        }

        jwt.refreshToken(payload)
        .then((session) => {
            resolve(utils.createResponse({
                "statusCode": 200,
                "body": session
            }));
        })
        .catch(err => {
            resolve(utils.formatErrorResponse(
                401,
                err
            ));
        });
    });
};