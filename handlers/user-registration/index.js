const bcrypt = require('bcryptjs');
const otp = require('/opt/nodejs/otp-layer/otp');
const users = require('/opt/nodejs/ddb-layer/users');
const utils = require('/opt/nodejs/utility-layer/utils');

const PURPOSE_REGISTRATION = "user registration";
const PURPOSE_PASSWORD_RESET = "password reset";
const SALT_ROUNDS = 10;

function sendRegistrationOTP({ email }) {
    return new Promise((resolve, reject) => {
        otp.createEntry({ email, purpose: PURPOSE_REGISTRATION })
        .then((value) => {
            otp.send({
                email,
                subject: "Confirm your Trophies account registration",
                body: `Please submit the following One-Time Password to confirm your registration:<br />
<br />
Your registration OTP: ${value}<br />
<br />
Your account will be inactive until it is confirmed.<br />
<br />
If your link has expired, please register again.<br />
If you did not intend to sign up, then it is safe to simply ignore this message.`
            })
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
}

function sendConfirmationNotification({ email }) {
    return new Promise((resolve, reject) => {
        otp.send({
            email,
            subject: `Your Trophies account has been activated`,
            body: `Thank you for confirming your account registration.
<br />
If you did not intend to register this email address, please contact the system administrator immediately.`
        })
        .then(resolve)
        .catch(reject);
    });
};

function sendPasswordResetNotification({ email }) {
    return new Promise((resolve, reject) => {
        otp.send({
            email,
            subject: `Your Trophies account password has been updated`,
            body: `Your account password has been updated.
<br />
If you did not intend to reset your password, please contact the system administrator immediately.`
        })
        .then(resolve)
        .catch(reject);
    });
};

function sendPasswordResetOTP({ email }) {
    return new Promise((resolve, reject) => {
        otp.createEntry({ email, purpose: PURPOSE_PASSWORD_RESET })
        .then((value) => {
            otp.send({
                email,
                subject: "Reset your Trophies account password",
                body: `Please submit the following One-Time Password to reset your password:<br />
<br />
Your password reset OTP: ${value}<br />
<br />
If your link has expired, please try again.<br />
If you did not intend to reset your password, then it is recommended to inform the system administrator as soon as possible.`
            })
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
}

function deleteOtp(payload) {
    return new Promise((resolve, reject) => {
        otp.deleteEntry(payload)
        .then(() => {
            console.log('OTP deleted successfully');
            resolve();
        })
        .catch(err => {
            console.error('OTP deletion failed')
            console.error(err);
            resolve();
        })
    })
};

exports.register = async (event) => {
    return new Promise((resolve, reject) => {
        try {
            let payload = utils.processEventBody({
                body: event.body,
                required: [ 'email', 'password' ]
            });

            // TODO zxcvbn
            // hash the password
            bcrypt.hash(payload.password, SALT_ROUNDS, function(err, hashedPassword) {
                if (err) {
                    console.error(`error hashing password ${payload.password}`, err);
                    return resolve(utils.formatErrorResponse(
                        500,
                        null,
                        `An unexpected error occurred.`
                    ));
                }

                payload.password = hashedPassword;
                users.createEntry(payload)
                .then(() => {
                    // create and send the confirmation OTP
                    // it's okay if we end up with multiple valid confirmation OTPs
                    sendRegistrationOTP({
                        email: payload.email,
                    })
                    .then(() => {
                        resolve(utils.createResponse({
                            "statusCode": 200,
                            "body": {
                                "success": true,
                                "message": "Please confirm your registration using the OTP sent to your email address."
                            }
                        }));
                    })
                    .catch((err) => {
                        console.error(err);
                        resolve(utils.formatErrorResponse(
                            500,
                            err,
                            `Registration of ${payload.email} failed.`
                        ));
                    });
                })
                .catch(err => {
                    console.error(`error creating entry for ${JSON.stringify(payload)}`, err);
                    return resolve(utils.formatErrorResponse(
                        500,
                        null,
                        err.message == "Email has already been registered." ?
                            err.message : `An unexpected error occurred.`
                    ));
                });
            });
        } catch (err) {
            console.error(err);
            resolve(utils.formatErrorResponse(
                400,
                err
            ));
        }
    });
};

exports.confirm = async (event) => {
    return new Promise((resolve, reject) => {
        let payload = null;
        try {
            payload = utils.processEventBody({
                body: event.body,
                required: ['email', 'otp']
            });
        } catch (err) {
            return resolve(utils.formatErrorResponse(400, err));
        }

        let confirmationFailureErrorHandler = (err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err,
                `Account confirmation failed, OTP invalid / expired.`
            ));
        };

        otp.getEntry({ email: payload.email, otp: payload.otp })
        .then(item => {
            // check otp has the correct purpose
            if (item.purpose != PURPOSE_REGISTRATION) {
                confirmationFailureErrorHandler(`OTP purpose mismatch: ${item.purpose}`);
            } else {
                users.getEntry(payload)
                .then(user => {
                    console.log(`users.getEntry`, user);
                    user.confirmed = true;
                    users.updateEntry(user)
                    .then(() => {
                        console.log(`email ${user.email} confirmed successfully`);
                        let promises = [];
                        promises.push(sendConfirmationNotification(user));
                        promises.push(deleteOtp(payload));
                        Promise.all(promises)
                        .then(() => {
                            resolve(utils.createResponse({
                                "statusCode": 200,
                                "body": {
                                    "success": true,
                                    "message": `Account confirmation succeeded.`
                                }
                            }));
                        })
                        .catch(err=>{
                            resolve(utils.formatErrorResponse(
                                500,
                                err,
                                `Account confirmation succeeded but email notifications not sent.`
                            ));
                        });
                    })
                    .catch(err => {
                        console.error(`An error occurred confirming ${payload.email}`, err);
                        deleteOtp(payload)
                        .then(() => {
                            resolve(utils.formatErrorResponse(
                                500,
                                err,
                                `Account confirmation failed.`
                            ));
                        });
                    });
                })
                .catch(err => {
                    console.error(`an error occurred while querying ${payload.email}`, err);
                    return resolve(utils.formatErrorResponse(
                        401,
                        null,
                        `An unexpected error occurred.`
                    ));
                });
            }
        })
        .catch(confirmationFailureErrorHandler);
    });
}

exports.requestPasswordReset = async (event) => {
    return new Promise((resolve, reject) => {
        try {
            let payload = utils.processEventBody({
                body: event.body,
                required: [ 'email' ]
            });

            let resolveSuccess = (msg) => {
                if (msg) {
                    console.error(msg);
                }
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true,
                        "message": `Please check your email inbox for the OTP required to reset your password.`
                    }
                }));
            };
            users.getEntry(payload)
            .then(user => {
                // create and send the password reset OTP
                // TODO is it okay if we end up with multiple valid confirmation OTPs?
                sendPasswordResetOTP({
                    email: payload.email,
                })
                .then(() => {
                    resolveSuccess();
                })
                .catch((err) => {
                    console.error(err);
                    resolve(utils.formatErrorResponse(
                        500,
                        err,
                        `Failed to send a password reset OTP, please contact the system administrator.`
                    ));
                });
            })
            .catch(err => {
                // pretend to succeed to prevent abusive email lookups
                resolveSuccess(
                    `an error occurred while querying ${payload.email}`, err
                );
            });
        } catch (err) {
            console.error(err);
            resolve(utils.formatErrorResponse(
                400,
                err
            ));
        }
    });
};

exports.completePasswordReset = async (event) => {
    return new Promise((resolve, reject) => {
        try {
            let payload = utils.processEventBody({
                body: event.body,
                required: [ 'email', 'password', 'otp' ]
            });

            let passwordResetFailureErrorHandler = (err) => {
                console.error(err);
                resolve(utils.formatErrorResponse(
                    401,
                    err,
                    `Password reset failed, OTP invalid / expired.`
                ));
            };

            otp.getEntry({ email: payload.email, otp: payload.otp })
            .then(item => {
                // check otp has the correct purpose
                if (item.purpose != PURPOSE_PASSWORD_RESET) {
                    passwordResetFailureErrorHandler(`OTP purpose mismatch: ${item.purpose}`);
                } else {
                    // TODO zxcvbn
                    // hash the password
                    bcrypt.hash(payload.password, SALT_ROUNDS, function(err, hashedPassword) {
                        if (err) {
                            console.error(`error hashing password ${payload.password}`, err);
                            return resolve(utils.formatErrorResponse(
                                500,
                                null,
                                `An unexpected error occurred.`
                            ));
                        }

                        users.getEntry(payload)
                        .then(user => {
                            user.password = hashedPassword;
                            users.updateEntry(user)
                            .then(() => {
                                let promises = [];
                                console.log(`email ${user.email} password reset successfully`);
                                promises.push(sendPasswordResetNotification(user));
                                promises.push(deleteOtp(payload));
                                Promise.all(promises)
                                .then(() => {
                                    resolve(utils.createResponse({
                                        "statusCode": 200,
                                        "body": {
                                            "success": true,
                                            "message": `Password has been updated successfully.`
                                        }
                                    }));
                                })
                                .catch(err=>{
                                    resolve(utils.formatErrorResponse(
                                        500,
                                        err,
                                        `Password reset succeeded but email notifications not sent.`
                                    ));
                                });
                            })
                            .catch(err => {
                                console.error(`An error occurred resetting password for ${payload.email}`, err);
                                deleteOtp(payload)
                                .then(() => {
                                    resolve(utils.formatErrorResponse(
                                        500,
                                        err,
                                        `Password reset failed.`
                                    ));
                                });
                            });
                        })
                        .catch(err => {
                            console.error(`an error occurred while querying ${payload.email}`, err);
                            return resolve(utils.formatErrorResponse(
                                401,
                                null,
                                `An unexpected error occurred.`
                            ));
                        });

                    });

                }
            })
            .catch(passwordResetFailureErrorHandler);

        } catch (err) {
            console.error(err);
            resolve(utils.formatErrorResponse(
                400,
                err
            ));
        }
    });
};

