import * as cdk from '@aws-cdk/core';
import { RestApi, LambdaIntegration, Cors } from '@aws-cdk/aws-apigateway';
import { Table, AttributeType, BillingMode } from '@aws-cdk/aws-dynamodb';
import { Function, Runtime, Code, LayerVersion } from '@aws-cdk/aws-lambda';
import { Rule, Schedule } from '@aws-cdk/aws-events';
import { LambdaFunction } from '@aws-cdk/aws-events-targets';

const DDB_ACCESS = {
    READ: 'read',
    WRITE: 'write',
    FULL: 'full'
};

export class TrophiesManagerStack extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps, customOptions?: any) {
        super(scope, id, props);
        const stack = this;
        customOptions = customOptions || {};

        // ddb for users / project owners
        //  email - string
        //  password - bcrypt hash of the user's password
        //  projects - array of uuids
        const userTable = new Table(stack, 'users', {
            partitionKey: { name: 'email', type: AttributeType.STRING },
            billingMode: BillingMode.PAY_PER_REQUEST
        });

        // otp entry will include
        //  email - string
        //  otp - string of unambiguous characters
        //  purpose - string
        //  TTL on expiration field
        const otpTable = new Table(stack, 'otp', {
            partitionKey: { name: 'email', type: AttributeType.STRING },
            sortKey: { name: 'otp', type: AttributeType.STRING },
            billingMode: BillingMode.PAY_PER_REQUEST,
            timeToLiveAttribute: 'expiration'
        });

        // refresh token entry will include
        //  email - string
        //  deviceId - uuid
        //  refreshToken - bcrypt hash of the refresh token
        //  TTL on expiration field
        const refreshTokenTable = new Table(stack, 'refresh-tokens', {
            partitionKey: { name: 'email', type: AttributeType.STRING },
            sortKey: { name: 'deviceId', type: AttributeType.STRING },
            billingMode: BillingMode.PAY_PER_REQUEST,
            timeToLiveAttribute: 'expiration'
        });

        // ddb for projects
        //  projectId - uuid
        //  owners - array of strings
        //  name - string
        //  repositories - array of repo objects { url, apiKey }
        //  tokensIssued - integer (9,007,199,254,740,991 seems sufficient)
        //  currency = string currency code
        //  totalFundsReceived - integer value of smallest currency denomination (ditto)
        const projectTable = new Table(stack, 'projects', {
            partitionKey: { name: 'projectId', type: AttributeType.STRING },
            billingMode: BillingMode.PAY_PER_REQUEST
        });

        // ddb for project funding history
        //  projectId - uuid
        //  funds - amount funded in smallest currency denomination
        //  source - string
        //  timestamp
        const projectFundingTable = new Table(stack, 'project-funding', {
            partitionKey: { name: 'projectId', type: AttributeType.STRING },
            sortKey: { name: 'timestamp', type: AttributeType.NUMBER },
            billingMode: BillingMode.PAY_PER_REQUEST
        });

        // ddb for trophies
        //    global secondary indexes: we want to search both by project and by token
        //  projectId - uuid
        //  trophyId - uuid
        //  sequenceNumber = string (1/1 or 1/3 or 3/3 etc.)
        //  author - string
        //  owner - string
        //  justification - string
        //  timestamp
        const trophyTable = new Table(stack, 'trophies', {
            partitionKey: { name: 'projectId', type: AttributeType.STRING },
            sortKey: { name: 'trophyId', type: AttributeType.STRING },
            billingMode: BillingMode.PAY_PER_REQUEST
        });

        const TROPHY_TABLE_GSI = 'trophy-table-trophyId-index';
        trophyTable.addGlobalSecondaryIndex({
            indexName: TROPHY_TABLE_GSI,
            partitionKey: { name: 'trophyId', type: AttributeType.STRING },
        });

        // ddb for trophy redemptions
        //  projectId - uuid
        //  claimId - uuid
        //  timestamp
        //  email - string
        //  trophies - array of { trophyId, amount }
        //  depositAccount - string
        //  isAuthorized - boolean
        //  isComplete - boolean
        const claimsTable = new Table(stack, 'trophy-redemptions', {
            partitionKey: { name: 'projectId', type: AttributeType.STRING },
            sortKey: { name: 'claimId', type: AttributeType.STRING },
            billingMode: BillingMode.PAY_PER_REQUEST
        });

        const CLAIMS_TABLE_GSI = 'redemptions-table-email-index';
        claimsTable.addGlobalSecondaryIndex({
            indexName: CLAIMS_TABLE_GSI,
            partitionKey: { name: 'email', type: AttributeType.STRING },
            sortKey: { name: 'claimId', type: AttributeType.STRING },
        });

        // utility layer
        const utilityLayer = new LayerVersion(stack, 'utility-layer', {
            // Code.fromAsset must reference the build folder
            code: Code.fromAsset('./layers/build/utility-layer'),
            compatibleRuntimes: [Runtime.NODEJS_12_X],
            license: 'MIT',
            description: 'a layer for providing utility functions',
        });

        // otp layer
        const otpLayer = new LayerVersion(stack, 'otp-layer', {
            // Code.fromAsset must reference the build folder
            code: Code.fromAsset('./layers/build/otp-layer'),
            compatibleRuntimes: [Runtime.NODEJS_12_X],
            license: 'MIT',
            description: 'a layer for providing otp functions',
        });

        // ddb layer
        const ddbLayer = new LayerVersion(stack, 'ddb-layer', {
            // Code.fromAsset must reference the build folder
            code: Code.fromAsset('./layers/build/ddb-layer'),
            compatibleRuntimes: [Runtime.NODEJS_12_X],
            license: 'MIT',
            description: 'a layer for providing dynamodb table interfaces',
        });

        // set default CORS origin to ALL_ORIGINS
        let corsOrigin = customOptions.corsOrigin || "*";
        // make the stack's CORS origin available to lambdas as an environment variable
        let corsEnvironment = {
            CORS_ORIGIN: corsOrigin
        };

        let userManagementEnvironment = {
            USERS_TABLE_NAME: userTable.tableName,
            REFRESH_TOKENS_TABLE_NAME: refreshTokenTable.tableName,
            OTP_TABLE_NAME: otpTable.tableName,
        };

        let trophyEnvironment = {
            PROJECTS_TABLE_NAME: projectTable.tableName,
            PROJECT_FUNDING_TABLE_NAME: projectFundingTable.tableName,
            TROPHIES_TABLE_NAME: trophyTable.tableName,
            USERS_TABLE_NAME: userTable.tableName,
            TROPHY_TABLE_GSI,
            CLAIMS_TABLE_NAME: claimsTable.tableName,
            CLAIMS_TABLE_GSI,
            OTP_TABLE_NAME: otpTable.tableName,
        };

        let jwtEnvironment = {
            JWT_SECRET: customOptions.jwt.secret,
            JWT_EXPIRATION: customOptions.jwt.authTokenExpiration,
            JWT_REFRESH_EXPIRATION: customOptions.jwt.refreshTokenExpiration,
        }

        let mailgunEnvironment = {
            MAILGUN_DOMAIN: customOptions.mailgun.domain,
            MAILGUN_FROM: customOptions.mailgun.from,
            MAILGUN_API_KEY: customOptions.mailgun.api_key
        }

        const trophiesApi = new RestApi(stack, `trophies-api`, {
            defaultCorsPreflightOptions: {
            allowOrigins: [ corsOrigin ],
            allowMethods: Cors.ALL_METHODS,
            }
        });

        // set up api resources
        const api: any = {
            'root': trophiesApi.root
        };

        // /register
        api.register = api.root.addResource('register');
        // /register/confirm
        api.registerConfirm = api.register.addResource('confirm');
        // /reset-password
        api.resetPassword = api.root.addResource('reset-password');
        // /reset-password/request
        api.resetPasswordRequest = api.resetPassword.addResource('request');

        let registrationFunctions = [
            {
                name: 'user-registration',
                handler: 'index.register',
                code: './handlers/user-registration',
                method: 'POST',
                resource: api.register,
                environment: {
                    ...corsEnvironment,
                    ...userManagementEnvironment,
                    ...mailgunEnvironment,
                    CLIENT_HOST: customOptions.clientUrl
                },
                ddbAccess: [
                    { table: userTable, access: DDB_ACCESS.FULL },
                    { table: otpTable, access: DDB_ACCESS.WRITE }
                ],
                layers: [ddbLayer, utilityLayer, otpLayer],
            },
            {
                name: 'user-confirmation',
                handler: 'index.confirm',
                code: './handlers/user-registration',
                method: 'POST',
                resource: api.registerConfirm,
                environment: {
                    ...corsEnvironment,
                    ...userManagementEnvironment,
                    ...mailgunEnvironment,
                    CLIENT_HOST: customOptions.clientUrl
                },
                ddbAccess: [
                    { table: otpTable, access: DDB_ACCESS.FULL },
                    { table: userTable, access: DDB_ACCESS.FULL },
                ],
                layers: [ddbLayer, utilityLayer, otpLayer],
            },
            {
                name: 'password-reset-request',
                handler: 'index.requestPasswordReset',
                code: './handlers/user-registration',
                method: 'POST',
                resource: api.resetPasswordRequest,
                environment: {
                    ...corsEnvironment,
                    ...userManagementEnvironment,
                    ...mailgunEnvironment,
                    CLIENT_HOST: customOptions.clientUrl
                },
                ddbAccess: [
                    { table: userTable, access: DDB_ACCESS.READ },
                    { table: otpTable, access: DDB_ACCESS.WRITE }
                ],
                layers: [ddbLayer, utilityLayer, otpLayer],
            },
            {
                name: 'password-reset',
                handler: 'index.completePasswordReset',
                code: './handlers/user-registration',
                method: 'POST',
                resource: api.resetPassword,
                environment: {
                    ...corsEnvironment,
                    ...userManagementEnvironment,
                    ...mailgunEnvironment,
                    CLIENT_HOST: customOptions.clientUrl
                },
                ddbAccess: [
                    { table: otpTable, access: DDB_ACCESS.FULL },
                    { table: userTable, access: DDB_ACCESS.FULL },
                ],
                layers: [ddbLayer, utilityLayer, otpLayer],
            },
        ];

        for (let rfi in registrationFunctions) {
            createIntegratedFunction(stack, registrationFunctions[rfi]);
        }

        // /login
        api.login = api.root.addResource('login');
        api.loginRefreshToken = api.login.addResource('refresh');

        let loginFunctions = [
            {
                name: 'user-login',
                handler: 'index.login',
                code: './handlers/user-authentication',
                method: 'POST',
                resource: api.login,
                environment: {
                    ...corsEnvironment,
                    ...userManagementEnvironment,
                    ...jwtEnvironment,
                },
                ddbAccess: [
                    { table: userTable, access: DDB_ACCESS.READ },
                    { table: refreshTokenTable, access: DDB_ACCESS.WRITE }
                ],
                layers: [ddbLayer, utilityLayer],
            },
            {
                name: 'user-refresh-token',
                handler: 'index.refreshToken',
                code: './handlers/jwt',
                method: 'POST',
                resource: api.loginRefreshToken,
                environment: {
                    ...corsEnvironment,
                    ...userManagementEnvironment,
                    ...jwtEnvironment,
                },
                ddbAccess: [
                    { table: userTable, access: DDB_ACCESS.READ },
                    { table: refreshTokenTable, access: DDB_ACCESS.FULL }
                ],
                layers: [ddbLayer, utilityLayer],
            },
        ];

        for (let lfi in loginFunctions) {
            createIntegratedFunction(stack, loginFunctions[lfi]);
        }

        // /projects
        api.projects = api.root.addResource('projects');
        // /projects/{projectId}
        api.project = api.projects.addResource('{projectId}');
        // /projects/{projectId}/claim
        api.projectClaim = api.project.addResource('claim');
        // /projects/{projectId}/funds
        api.projectFunds = api.project.addResource('funds');
        // /projects/{projectId}/funds/renewToken
        api.projectFundsRenewToken = api.projectFunds.addResource('renewToken');
        // /projects/{projectId}/repositories
        api.projectRepositories = api.project.addResource('repositories');
        // /projects/{projectId}/repositories/{repositoryId}
        api.projectRepository = api.projectRepositories.addResource('{repositoryId}');

        let projectFunctions = [
            {
                name: 'create-project',
                handler: 'index.createProject',
                code: './handlers/projects',
                method: 'POST',
                resource: api.projects,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                    ...jwtEnvironment,
                },
                ddbAccess: [
                { table: projectTable, access: DDB_ACCESS.FULL },
                { table: userTable, access: DDB_ACCESS.FULL },
                ],
                layers: [ddbLayer, utilityLayer],
            },
            {
                name: 'fund-project',
                handler: 'index.fundProject',
                code: './handlers/projects',
                method: 'POST',
                resource: api.projectFunds,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                },
                ddbAccess: [
                    { table: projectTable, access: DDB_ACCESS.FULL },
                    { table: projectFundingTable, access: DDB_ACCESS.WRITE }
                ],
                layers: [ddbLayer, utilityLayer],
            },
            {
                name: 'renew-project-fund-api-key',
                handler: 'index.renewFundApiKey',
                code: './handlers/projects',
                method: 'POST',
                resource: api.projectFundsRenewToken,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                    ...jwtEnvironment,
                },
                ddbAccess: [
                    { table: projectTable, access: DDB_ACCESS.FULL },
                ],
                layers: [ddbLayer, utilityLayer],
            },
            {
                name: 'claim-project',
                handler: 'index.claimProject',
                code: './handlers/projects',
                method: 'POST',
                resource: api.projectClaim,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                    ...jwtEnvironment,
                },
                ddbAccess: [
                    { table: userTable, access: DDB_ACCESS.FULL },
                    { table: projectTable, access: DDB_ACCESS.READ }
                ],
                layers: [ddbLayer, utilityLayer],
            },
            {
                name: 'get-project',
                handler: 'index.getProject',
                code: './handlers/projects',
                method: 'GET',
                resource: api.project,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                    ...jwtEnvironment,
                },
                ddbAccess: [
                    { table: projectTable, access: DDB_ACCESS.READ }
                ],
                layers: [ddbLayer, utilityLayer],
                },
                {
                name: 'list-projects',
                handler: 'index.listProjects',
                code: './handlers/projects',
                method: 'GET',
                resource: api.projects,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                    ...jwtEnvironment,
                },
                ddbAccess: [
                    { table: projectTable, access: DDB_ACCESS.READ }
                ],
                layers: [ddbLayer, utilityLayer],
                },
                {
                name: 'add-repository',
                handler: 'repositories.addRepository',
                code: './handlers/projects',
                method: 'POST',
                resource: api.projectRepositories,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                    ...jwtEnvironment,
                },
                ddbAccess: [
                    { table: projectTable, access: DDB_ACCESS.FULL },
                ],
                layers: [ddbLayer, utilityLayer],
            },
            {
                name: 'renew-repository-api-key',
                handler: 'repositories.renewApiKey',
                code: './handlers/projects',
                method: 'PUT',
                resource: api.projectRepository,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                    ...jwtEnvironment,
                },
                ddbAccess: [
                    { table: projectTable, access: DDB_ACCESS.FULL },
                ],
                layers: [ddbLayer, utilityLayer],
            },
            {
                name: 'delete-repository',
                handler: 'repositories.removeRepository',
                code: './handlers/projects',
                method: 'DELETE',
                resource: api.projectRepository,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                    ...jwtEnvironment,
                },
                ddbAccess: [
                    { table: projectTable, access: DDB_ACCESS.FULL },
                ],
                layers: [ddbLayer, utilityLayer],
            },
        ];

        for (let pfi in projectFunctions) {
            createIntegratedFunction(stack, projectFunctions[pfi]);
        }

        // /trophies
        api.trophies = api.root.addResource('trophies');
        // /trophies/redeem
        api.trophiesRedeem = api.trophies.addResource('redeem');
        // /trophies/{trophyId}
        api.trophy = api.trophies.addResource('{trophyId}');
        // /projects/{projectId}/trophies
        api.projectTrophies = api.project.addResource('trophies');

        let trophyFunctions = [
            {
                name: 'issue-trophies',
                handler: 'issue.issueTrophies',
                code: './handlers/trophies',
                method: 'POST',
                resource: api.projectTrophies,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                    ...jwtEnvironment,
                    ...mailgunEnvironment,
                },
                ddbAccess: [
                    { table: projectTable, access: DDB_ACCESS.FULL },
                    { table: trophyTable, access: DDB_ACCESS.WRITE },
                ],
                layers: [ddbLayer, otpLayer, utilityLayer],
            },
            {
                name: 'get-trophy',
                handler: 'index.getTrophy',
                code: './handlers/trophies',
                method: 'GET',
                resource: api.trophy,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                },
                ddbAccess: [
                    { table: trophyTable, access: DDB_ACCESS.READ }
                ],
                layers: [ddbLayer, utilityLayer],
            },
            {
                name: 'list-project-trophies',
                handler: 'index.listProjectTrophies',
                code: './handlers/trophies',
                method: 'GET',
                resource: api.projectTrophies,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                },
                ddbAccess: [
                    { table: trophyTable, access: DDB_ACCESS.READ }
                ],
                layers: [ddbLayer, utilityLayer],
            },
            {
                name: 'redeem-trophies',
                handler: 'claims.requestTrophyRedemption',
                code: './handlers/trophies',
                method: 'POST',
                resource: api.trophiesRedeem,
                environment: {
                    ...corsEnvironment,
                    ...trophyEnvironment,
                    ...jwtEnvironment,
                    ...mailgunEnvironment,
                },
                ddbAccess: [
                    { table: projectTable, access: DDB_ACCESS.READ },
                    { table: trophyTable, access: DDB_ACCESS.READ },
                    { table: claimsTable, access: DDB_ACCESS.WRITE },
                    { table: otpTable, access: DDB_ACCESS.WRITE }
                ],
                layers: [ddbLayer, otpLayer, utilityLayer],
                timeout: cdk.Duration.seconds(30),
            },
        ];

        for (let tfi in trophyFunctions) {
            createIntegratedFunction(stack, trophyFunctions[tfi]);
        }
    }
}

type ddbAccessConfiguration = {
    table: Table,
    access: string
}

function createIntegratedFunction(stack: TrophiesManagerStack, definition: any) {
    console.log(`creating lambda ${definition.name}...`);
    let lambda = new Function(stack, `${definition.name}-function`, {
        runtime: Runtime.NODEJS_12_X,
        handler: definition.handler,
        code: Code.fromAsset(definition.code),
        environment: definition.environment,
        layers: definition.layers,
        timeout: definition.timeout || cdk.Duration.seconds(5)
    });

    if (!definition.methods && definition.method) {
        definition.methods = [ definition.method ];
    }

    if (!definition.resources && definition.resource) {
        definition.resources = [ definition.resource ];
    }

    let lambdaIntegration = new LambdaIntegration(lambda);
    for (let ri in definition.resources) {
        let resource = definition.resources[ri];
        for (let mi in definition.methods) {
            let method = definition.methods[mi];
            console.log(`adding method ${method} to resource ${resource}...`);
            resource.addMethod(method, lambdaIntegration);
        }
    }

    let ddbAccessConfigurations: ddbAccessConfiguration[] = definition.ddbAccess || [];
    for (let dai in ddbAccessConfigurations) {
        let ddbAccessConfiguration = ddbAccessConfigurations[dai];
        console.log(`granting ${definition.name} ${ddbAccessConfiguration.access} access on ${ddbAccessConfiguration.table.tableName}...`)
        switch (ddbAccessConfiguration.access) {
            case DDB_ACCESS.FULL:
                ddbAccessConfiguration.table.grantFullAccess(lambda);
                break;
            case DDB_ACCESS.READ:
                ddbAccessConfiguration.table.grantReadData(lambda);
                break;
            case DDB_ACCESS.WRITE:
                ddbAccessConfiguration.table.grantWriteData(lambda);
                break;
        }
    }

    let queueAccess = definition.queueAccess || [];
    for (let qai in queueAccess) {
        definition.queueAccess[qai].grantSendMessages(lambda);
    }

    let eventSources = definition.eventSources || [];
    for (let esi in eventSources) {
        lambda.addEventSource(eventSources[esi]);
    }

    if (definition.cron) {
        let rule = new Rule(stack, `${definition.name}-rule`, {
            schedule: definition.cron
        });

        rule.addTarget(new LambdaFunction(lambda));
    }
}
